var TodoControllers = angular.module('TodoControllers', []);

TodoControllers.controller('TodoListCtrl', ['$scope', '$dragon', function ($scope, $dragon) {
    $scope.channel = 'notifications';

    $dragon.onReady(function() {
        $dragon.subscribe('notifications', $scope.channel ).then(function(response) {
            $scope.dataMapper = new DataMapper(response.data);
            console.log(response);
        });

      //  $dragon.getSingle('notifications', { pk:1 } ).then(function(response) {
        //    $scope.notifications = [response.data];
          //  console.log($scope.notifications);
       // });

      // $dragon.getList('notifications').then(function(response) {
        //    $scope.todoItems = response.data;
          //  console.log(response);
       //});
    });

    $dragon.onChannelMessage(function(channels, message) {
        if (indexOf.call(channels, $scope.channel) > -1) {
            $scope.$apply(function() {
                $scope.dataMapper.mapData($scope.notifications, message);
                addNotification((message.data));
            });
        }
        console.log(channels + ' ' + message);
    });

    $scope.itemDone = function(item) {
        item.done = true != item.done;
        $dragon.update('notifications', item);
    };
    
    function addNotification(notification) {
        if (window.Notification && Notification.permission === "granted") {
            new Notification(notification.message);
        }

    }
}]);


window.addEventListener('load', function () {
    Notification.requestPermission(function (status) {
        if (Notification.permission !== status) {
            Notification.permission = status;
        }
    });
});