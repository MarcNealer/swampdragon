from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework import status
from .serializers import *
from .models import *
# Create your views here.
from django.views.generic import ListView



class Notifications(ListView):
    model = Notification
    template_name = 'index1.html'

    def get_queryset(self):
        return self.model.objects.order_by('-pk')[:5]

class TopicViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = TopicSerializer
    queryset = Topic.objects.all()

class SubscriptionViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = SubscriptionSerializer
    queryset = Subscription.objects.all()

class NotificationViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = NotificationSerializer
    queryset = Notification.objects.all()
    @detail_route(methods=['GET'])
    def foruser(self,request, pk=None):
        '''
        returns a list of notifications for a given user where pk=UserId
        '''
        if not pk:
            return Response('no user id specified',status=status.HTTP_400_BAD_REQUEST)
        recs = Notification.objects.filter(subscriber_user_id=pk)
        ser = NotificationSerializer(recs,many=True)
        return Response(ser.data)
    @detail_route(methods=['GET'])
    def createdby(self,request,pk=None):
        '''
        returns a list of notifications created by a give user where pk=UserId
        '''
        if not pk:
            return Response('no user id specified',status=status.HTTP_400_BAD_REQUEST)
        recs = Notification.objects.filter(publisher_user_id=pk)
        ser = NotificationSerializer(recs,many=True)
        return Response(ser.data)

class ArchiveViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = ArchiveSerializer
    queryset = NotificationsArchive.objects.all()
    @detail_route(methods=['GET'])
    def foruser(self,request, pk=None):
        '''
        returns a list of archived notifications for a seleted user
        :param request:
        :param pk: user id
        :return:
        '''
        if not pk:
            return Response('no user id specified',status=status.HTTP_400_BAD_REQUEST)
        recs = NotificationsArchive.objects.filter(subscriber_user_id=pk)
        ser = ArchiveSerializer(recs,many=True)
        return Response(ser.data)
    @detail_route(methods=['GET'])
    def createdby(self,request,pk=None):
        '''
        returns archived notifications created by a given user. pk = Userid
        '''
        if not pk:
            return Response('no user id specified',status=status.HTTP_400_BAD_REQUEST)
        recs = NotificationsArchive.objects.filter(publisher_user_id=pk)
        ser = ArchiveSerializer(recs,many=True)
        return Response(ser.data)