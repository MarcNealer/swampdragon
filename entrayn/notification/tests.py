from django.test import TestCase
from rest_framework.test import APIClient
# Create your tests here.

class PublisherTestCase(TestCase):
    """
    Tests for publisher records.
    """
    fixtures = ['testdata']

    def setUp(self):
        pass

    #todo create topic
    #todo retrieve topic
    #todo update topic
    #todo delete topic
    #todo create subscription for a given user
    #todo get subscriptions for a topic
    #todo get subscriptions for a given user
    #todo create a notification
    #todo get notifications created by a selected user
    #todo get notifications for a selected user
    #todo delete a notification
    #todo update a notification
    #todo get archived notifications for a selected user