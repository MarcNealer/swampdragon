from rest_framework import serializers
from .models import *

class TopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Topic

class SubscriptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subscription

class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification

class ArchiveSerializer(serializers.ModelSerializer):
    class Meta:
        model = NotificationsArchive