from swampdragon import route_handler
from swampdragon.route_handler import ModelRouter
from .ws_serializers import *
from .models import Notification


class NotificationRouter(ModelRouter):
    serializer_class = NotificationSerializer
    model = Notification
    route_name = 'notifications'

    def get_object(self, **kwargs):
        return self.model.objects.get(pk=kwargs['pk'])

    def get_query_set(self, **kwargs):
        return self.model.filter()


route_handler.register(NotificationRouter)