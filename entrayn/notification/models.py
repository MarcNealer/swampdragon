from django.db import models
from django.contrib.auth.models import User
from django_extensions.db.fields.json import JSONField
from swampdragon.models import SelfPublishModel
from .ws_serializers import *
# Create your models here.

class UserAb(models.Model):
    """
    Abstract model for adding Users to the system
    """
    user_id = models.IntegerField()

    class Meta:
        abstract = True

class TimesAb(models.Model):
    """
    Abstract model for adding time stamps
    """
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

class Topic(SelfPublishModel,UserAb,TimesAb):
    """
    Topics that users can subscribe to
    """
    serializer_class = TopicSerializer
    name=models.CharField(max_length=250)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name

class Subscription(UserAb,TimesAb):
    """
    subscriber list model

    This is a relationship model connecting Users to topics as
    publishers or subscribers
    """
    topic_id = models.ForeignKey("Topic")

    def __str__(self):
        return "%s(%s) - %s" % (self.user_id,self.subscription_type,self.topic_id.name)


class Notification(SelfPublishModel,TimesAb):
    """
    Notifications

    A notification record is created for each user that has subscribed to
    receive a message form a selected topic
    """
    serializer_class = NotificationSerializer
    topic_id = models.ForeignKey("Topic")
    subscriber_user_id = models.IntegerField(null=True)
    publisher_user_id = models.IntegerField(null=True)
    entity_id = models.IntegerField(null=True)
    entity_type = models.CharField(max_length=15,blank=True)
    title = models.CharField(max_length=250,blank=True)
    message = models.TextField(blank=True)
    message_type = models.CharField(max_length=15,blank=True)
    message_actions = JSONField()
    message_status = models.IntegerField(null=True)

    #def __str__(self):
    #    return "Message:%s To:%s" % (self.title,self.subscriber_user_id)

class NotificationsArchive(Notification):
    '''
    Archived messages
    '''
    def __str__(self):
        return "Message:%s To:%s" % (self.title,self.subscriber_user_id)

