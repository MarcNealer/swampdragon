from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(Topic)
admin.site.register(Subscription)
admin.site.register(Notification)
admin.site.register(NotificationsArchive)