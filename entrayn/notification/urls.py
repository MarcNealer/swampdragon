from django.conf.urls import url,include
from .views import TopicViewSet, SubscriptionViewSet, NotificationViewSet, ArchiveViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'topic', TopicViewSet)
router.register(r'subscripion',SubscriptionViewSet)
router.register(r'notification',NotificationViewSet)
router.register(r'archive',ArchiveViewSet)
urlpatterns = router.urls