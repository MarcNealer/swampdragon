from swampdragon.serializers.model_serializer import ModelSerializer

class TopicSerializer(ModelSerializer):
    notification_set = 'notification.NotificationSerializer'
    class Meta:
        model = 'notification.Topic'

class NotificationSerializer(ModelSerializer):
    topic_id = TopicSerializer
    class Meta:
        model = 'notification.Notification'


